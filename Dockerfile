#
# GO-CSV-API Dockerfile
#

# Pull the base image
FROM golang:1.11-alpine

ENV APP_PATH /go/src/gitlab.com/raaz.crzy/go-csv-api

LABEL maintainer "Prince Raj <raaz.crzy@gmail.com>"

# Set GOPATH
ENV GOPATH /go

# Make directories for api_frontend
RUN mkdir -p ${APP_PATH}

# Define working directory
WORKDIR  ${APP_PATH}

# Add go-csv-api files
ADD .  ${APP_PATH}

RUN \
	cd ${APP_PATH} && \
	go install

# Define default command
CMD ["/go/bin/go-csv-api"]

# Expose Ports
EXPOSE 8080
