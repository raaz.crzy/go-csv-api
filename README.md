# go-csv-api

## Purpose

Assignment completion

## Description

This project reads from a CSV file and exposes a GET endpoint to explore the file. A dummy file is inside the repository. You can update the file with a new file keeping the filename same or updating the file name in main function and match function.

### Exposed endpoints

1. GET {{host}}:8080/:key

example:
Request: GET http://localhost:8080/one

User queries against a key and the record matching the key is sent back in the response. On successful match the following response is sent back

Response type: `application/json`

Response format:

 ```JS
{
    "key": "one",
    "value": 1
}
 ```

If a match doesnot happen, status 404 is sent back along with the message 'key not found'.

Example 404 response:

 ```JS
{
    "message": "key not found"
}
 ```

If the user requests an undefined path or uses undefined HTTP verb proper message is sent back.

## Build commands

The docker file included can be used to build a docker image. The command for the same is as follows:

`docker build --rm -f Dockerfile -t {{you tag name without these braces}} .`

Additionally, the project can also be run with `go run` command.

## Tests

The repository includes a test file with two test cases.

1. TestMatch_Positive: It tests for the match to happen and expects a positive result. i.e, the match must happen.

2. TestMatch_Negative: It tests for the match to not happen and expects a negative result. i.e, the match must not happen.

The command to run the test is:

`go test -v utils_test.go utils.go`