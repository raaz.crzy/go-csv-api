package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/valyala/fasthttp"
)

//getValueHandler handlers the request for GET /:key route
func getValueHandler(ctx *fasthttp.RequestCtx) {
	key := ctx.UserValue("key")
	//fetch the record corresponsing to the key
	matchedResult, err := match(fmt.Sprint(key))
	if err != nil {
		//if record is not found, send 404 status code with message
		if err.Error() == "not found" {
			ctx.Response.SetStatusCode(fasthttp.StatusNotFound)
			ctx.Response.Header.Set("Content-Type", "application/json")
			ctx.Response.SetBody([]byte("{\"message\":\"key not found\"}"))
			return
		}
	}
	//marshal the matchedResult to convert it into []byte
	response, err := json.Marshal(matchedResult)
	if err != nil {
		log.Println("error while marshalling matchedResult: ", err)
	}
	//set content-type, status code of the response and send the response with matched record
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(response)
	return
}
