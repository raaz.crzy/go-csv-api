package main

import (
	"log"
	"os"

	"github.com/valyala/fasthttp"
)

//start main function
func main() {
	//check if the file is accessible
	csvfile, err := os.Open("./test.CSV.XLSX")
	if err != nil {
		log.Fatalln("unable to open file: ", err)
	}
	defer csvfile.Close()
	router := getRouter()
	log.Println("Starting server at port 8080")
	fasthttp.ListenAndServe(":8080", router.Handler)
}
