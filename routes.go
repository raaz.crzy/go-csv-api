package main

import (
	"fmt"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

//getRouter returns the defined routes and their respective handlers
func getRouter() *fasthttprouter.Router {
	fmt.Println("Inside getRouter")
	router := fasthttprouter.New()
	router.GET("/:key", getValueHandler)
	router.HandleMethodNotAllowed = true
	router.MethodNotAllowed = methodNotAllowedHandler
	router.NotFound = urlNotFoundHandler
	return router
}

//methodNotAllowedHandler sends appropriate message if the requested HTTP method is not allowed
func methodNotAllowedHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetStatusCode(fasthttp.StatusForbidden)
	ctx.Response.SetBody([]byte("{\"message\":\"Method not allowed\"}"))
}

//urlNotFoundHandler sends appropriate message if the requested path is not defined in the routes
func urlNotFoundHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetStatusCode(fasthttp.StatusNotFound)
	ctx.Response.SetBody([]byte("{\"message\":\"URL not found\"}"))
}
