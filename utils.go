package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

// processCSV reads the csv one row at a time and sends the data on a channel.
// This helps avoid loading the full file in memory in one go
func processCSV(rc io.Reader) (ch chan []string) {
	ch = make(chan []string, 2)
	go func() {
		r := csv.NewReader(rc)
		r.LazyQuotes = true
		if _, err := r.Read(); err != nil { //read header
			log.Println("error while reading file: ", err)
		}

		defer close(ch)
		for {
			rec, err := r.Read()
			if err != nil {
				if err == io.EOF {
					fmt.Println("end of file")
					break
				}
				log.Println("Error while reading record: ", err)
			}
			ch <- rec
		}
	}()
	return
}

//struct definition for storing matched records
type result struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

//match function takes in a Key and matches it with the records in CSV file.
// If a match is found it sends back the corresponding record,
// else it sends back an error message 'not found'
func match(keyToMatch string) (result, error) {
	matchedResult := result{}
	csvfile, err := os.Open("./test.CSV.XLSX")

	if err != nil {
		fmt.Println("unable to open file: ", err)
		return matchedResult, err
	}
	defer csvfile.Close()
	for rec := range processCSV(csvfile) {
		key := rec[0]
		var err error
		var value interface{}
		if keyToMatch == key {
			value, err = strconv.ParseInt(rec[1], 10, 64)
			if err != nil {
				value = rec[1]
			}
			matchedResult.Key = key
			matchedResult.Value = value
			break
		}

	}
	if matchedResult.Key == "" {
		return matchedResult, errors.New("not found")
	}
	return matchedResult, nil
}
