package main

import (
	"testing"
)

//TestMatch_Positive assumes key 'one' is present in the file and hence match should happen
func TestMatch_Positive(t *testing.T) {
	key := "one"
	var expectedResult, actualResult bool
	_, err := match(key)
	if err != nil {
		if err.Error() == "not found" {
			actualResult = false
		}
	} else {
		actualResult = true
	}
	expectedResult = true

	if actualResult != expectedResult {
		t.Fatalf("1. Expected %v but got %v", expectedResult, actualResult)
	}
}

//TestMatch_Negative assumes key 'thousand' is not present in the file and hence no match should happen
func TestMatch_Negative(t *testing.T) {
	key := "thousand"
	var expectedResult, actualResult bool
	_, err := match(key)
	if err != nil {
		if err.Error() == "not found" {
			actualResult = false
		}
	} else {
		actualResult = false
	}
	expectedResult = false

	if actualResult != expectedResult {
		t.Fatalf("1. Expected %v but got %v", expectedResult, actualResult)
	}
}
